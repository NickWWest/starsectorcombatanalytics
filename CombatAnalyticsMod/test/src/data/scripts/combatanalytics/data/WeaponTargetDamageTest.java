package data.scripts.combatanalytics.data;

import com.fs.starfarer.api.combat.ShipAPI;
import org.junit.Assert;
import org.junit.Test;

public class WeaponTargetDamageTest {
    @Test
    public void TestCompare(){
        Ship player = new Ship("player", "playership", ShipAPI.HullSize.CRUISER, "player class", 100f, 200f, 1, 7, "cap1", "hullId1",  ShipStatus.DESTROYED, "c1", 333f);
        Ship compy = new Ship("compy",  "compy ship", ShipAPI.HullSize.CRUISER, "compy class", 100f, 200f, 0, 8, "cap2", "hullId2", ShipStatus.OK, "c2", 333f);

        WeaponShipDamage wd1 = new WeaponShipDamage(player, compy, "groupName 1");
        WeaponShipDamage wd2 = new WeaponShipDamage(player, compy, "groupName 2");


        Assert.assertTrue(wd1.compareTo(wd2) > 0);
    }
}
