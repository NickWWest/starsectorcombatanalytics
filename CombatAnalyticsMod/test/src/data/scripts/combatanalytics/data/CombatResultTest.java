package data.scripts.combatanalytics.data;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import data.scripts.combatanalytics.util.Helpers;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CombatResultTest {

    @Test
    public void testSort(){
        CombatResult cr1 = new CombatResult("combatId", "faction", "fleetname", 0, new Damage[0], 100f, CombatGoal.BATTLE, new ArrayList<Ship>());
        CombatResult cr2 = new CombatResult("combatId", "faction", "fleetname", 1, new Damage[0], 100f, CombatGoal.BATTLE, new ArrayList<Ship>());

        Assert.assertEquals(1, cr1.compareTo(cr2));
    }
}
