package data.scripts;


import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Serializes DamageReport data onto the data bus that is `Global.getCombatEngine().getCustomData()`
 * No actual custom objects are shared between mods to avoid strange dependency issues.
 *
 * DamageReportManager is a facade over the data bus for ease of use.
 */
public class DamageReportManagerV1 {

    private static final String DamageReportManagerKey = "DamageReportManagerV1";

    private final static Logger log = Global.getLogger(data.scripts.DamageReportManagerV1.class);

    private static List<Object[]> getDamageReportStream(){
        Map<String, Object> customData = Global.getCombatEngine().getCustomData();
        Object raw = customData.get(DamageReportManagerKey);
        DamageReportManagerV1 ret;
        if(raw == null){
            raw = new ArrayList<Object[]>(200);
            customData.put(DamageReportManagerKey, raw);
        }

        if(!(raw instanceof List)) {
            throw new RuntimeException("Unknown class for CustomDataKey: '"+ DamageReportManagerKey +"' class: '"+raw.getClass() +"'");
        }

        return (List<Object[]>)raw;
    }

    // static methods only
    private DamageReportManagerV1(){}

    public static void addDamageReport(DamageReportV1 dr){
        getDamageReportStream().add(DamageReportV1.serialize(dr));
    }

    public static void addDamageReport(float armorDamage, float hullDamage, float empDamage, float shieldDamage, DamagingProjectileAPI projectile) {
        try {
            addDamageReport(armorDamage, hullDamage, empDamage, shieldDamage, projectile.getDamageType(), projectile.getSource(), projectile.getDamageTarget(), projectile.getWeapon().getDisplayName());
        } catch (Exception e){
            log.warn("Error adding damage report", e);
        }
    }

    public static void addDamageReport(float armorDamage, float hullDamage, float empDamage, float shieldDamage, BeamAPI beam) {
        try{
            addDamageReport(armorDamage, hullDamage, empDamage, shieldDamage, beam.getDamage().getType(), beam.getSource(), beam.getDamageTarget(), beam.getWeapon().getDisplayName());
        } catch (Exception e){
            log.warn("Error adding damage report", e);
        }
    }

    public static void addDamageReport(float armorDamage, float hullDamage, float empDamage, float shieldDamage, DamageType damageType, CombatEntityAPI source, CombatEntityAPI target, String weaponName) {
        try{
            getDamageReportStream().add(new Object[]{armorDamage, hullDamage, empDamage, shieldDamage, damageType, source, target, weaponName});
        } catch (Exception e){
            log.warn("Error adding damage report", e);
        }
    }

    public static void addDamageClarification(float shipDamage, float empDamage, DamageType damageType, CombatEntityAPI source, CombatEntityAPI target, String weaponName){
        try{
            getDamageReportStream().add(new Object[]{shipDamage, empDamage, damageType, source, target, weaponName});
        } catch (Exception e){
            log.warn("Error adding damage clarification", e);
        }
    }

    public static void addDamageClarification(float shipDamage, float empDamage, DamagingProjectileAPI projectile, CombatEntityAPI target){
        try {
            addDamageClarification(shipDamage, empDamage, projectile.getDamageType(), projectile.getSource(), target, projectile.getWeapon().getDisplayName());
        } catch (Exception e){
            log.warn("Error adding damage report", e);
        }
    }

    public static void addDamageClarification(float shipDamage, float empDamage, DamagingProjectileAPI projectile){
        try {
            addDamageClarification(shipDamage, empDamage, projectile.getDamageType(), projectile.getSource(), projectile.getDamageTarget(), projectile.getWeapon().getDisplayName());
        } catch (Exception e){
            log.warn("Error adding damage report", e);
        }
    }

    public static List<DamageReportV1> getDamageReports(){
        List<Object[]> damageReports = getDamageReportStream();
        if(damageReports.size() == 0){
            return (List<DamageReportV1>)Collections.EMPTY_LIST;
        }

        List<DamageReportV1> ret = new ArrayList<>(damageReports.size());
        for(Object[] raw : damageReports){
            if(raw != null && raw.length == 8) {
                try {
                    DamageReportV1 dc = DamageReportV1.deserialize(raw);
                    if (dc.isValid()) {
                        ret.add(dc);
                    }
                } catch (Exception e){
                    log.warn("Error deserializing DamageReportV1", e);
                }
            }
        }

        return ret;
    }

    public static List<DamageClarificationV1> getDamageClarifications(){
        List<Object[]> damageReports = getDamageReportStream();
        if(damageReports.size() == 0){
            return (List<DamageClarificationV1>)Collections.EMPTY_LIST;
        }

        List<DamageClarificationV1> ret = new ArrayList<>(damageReports.size());
        for(Object[] raw : damageReports){
            if(raw != null && raw.length == 6) {
                try {
                    DamageClarificationV1 dc = DamageClarificationV1.deserialize(raw);
                    if (dc.isValid()) {
                        ret.add(dc);
                    }
                } catch (Exception e){
                    log.warn("Error deserializing DamageClarificationV1", e);
                }
            }
        }

        return ret;
    }

    public static void clearDamageReports(){
        getDamageReportStream().clear();
    }
}