package data.scripts.combatanalytics.data;

import com.fs.starfarer.api.Global;
import org.apache.log4j.Logger;

import static data.scripts.combatanalytics.util.Helpers.INT_FORMAT;

// A basic summary of how much damage a source ships groupName did to one target ship.
// Our most basic unit of damage, constructed post-combat by aggregating Damage objects
// as Damage objects are time based, this is battle based
public class WeaponShipDamage implements Comparable<WeaponShipDamage> {
    private static final Logger log = Global.getLogger(WeaponShipDamage.class);

    public Ship source;
    public Ship target;
    public String weapon;

    public int hits = 0;
    public double shieldDamage = 0;
    public double armorDamage = 0;
    public double hullDamage = 0;
    public double empDamage = 0;

    public double pctOfDamageDoneToTarget;
    public int killingBlow = 0;

    private WeaponShipDamage(){

    }

    public WeaponShipDamage(Ship source, Ship target, String weapon){
        this.source = source;
        this.target = target;
        this.weapon = weapon;
    }

    public boolean isHealing(){
        return (shieldDamage + empDamage + armorDamage + hullDamage) < 0;
    }

    public void merge(Damage d){
        hits ++;

        this.shieldDamage += d.shieldDamage;
        this.armorDamage += d.armorDamage;
        this.hullDamage += d.hullDamage;
        this.empDamage += d.empDamage;
        this.killingBlow += d.wasKillingBlow ? 1 : 0;
    }

    @Override
    public int compareTo(WeaponShipDamage o) {
        // name, groupName
        int compare = o.target.name.compareTo(this.target.name);
        if(compare != 0){
            return compare;
        }

        compare = o.source.name.compareTo(this.source.name);
        if(compare != 0){
            return compare;
        }

        return o.weapon.compareTo(this.weapon);
    }

    @Override
    public String toString(){
        return String.format("%2$s-->%3$s-->%4$s  %5$s shield dmg   %6$s armor dmg   %7$s hull dmg   %8$s flux dmg",
                0f, source, weapon, target,
                INT_FORMAT.format(shieldDamage), INT_FORMAT.format(armorDamage), INT_FORMAT.format(hullDamage), INT_FORMAT.format(empDamage));
    }

    private Object readResolve() {
        this.weapon = weapon.intern();

        return this;
    }

    public void prepForSave(){
        this.shieldDamage = (int) shieldDamage;
        this.armorDamage = (int) armorDamage;
        this.hullDamage = (int) hullDamage;
        this.empDamage = (int) empDamage;

        pctOfDamageDoneToTarget = Math.round(pctOfDamageDoneToTarget * 1000d) / 1000d;
    }
}
