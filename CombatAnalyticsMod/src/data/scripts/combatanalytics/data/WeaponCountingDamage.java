package data.scripts.combatanalytics.data;

/**
 * This class tracks binary outcomes that can be counted, rather than % of damages that can be shared
 */
public class WeaponCountingDamage implements Comparable<WeaponCountingDamage> {
    public Ship source;
    public String weapon;
    public int missileKills;
    public int fighterKills;

    private WeaponCountingDamage(){

    }

    public WeaponCountingDamage(Ship source, String weapon, int missileKills, int fighterKills){
        this.source = source;
        this.weapon = weapon;
        this.missileKills = missileKills;
        this.fighterKills = fighterKills;
    }

    @Override
    public String toString() {
        return "WeaponCountingDamage{" +
                "source=" + source.hullClass +
                ", weapon='" + weapon + '\'' +
                ", missileKills=" + missileKills +
                ", fighterKills=" + fighterKills +
                '}';
    }

    @Override
    public int compareTo(WeaponCountingDamage o) {
        // name, groupName
        int compare = o.source.name.compareTo(this.source.name);
        if(compare != 0){
            return compare;
        }

        return o.weapon.compareTo(this.weapon);
    }

    private Object readResolve() {
        this.weapon = weapon.intern();

        return this;
    }
}
