package data.scripts.combatanalytics.data;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import data.scripts.combatanalytics.util.Helpers;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

import static data.scripts.combatanalytics.util.Helpers.coalesce;
import static data.scripts.combatanalytics.util.Helpers.ownerAsString;

/**
 * Represents A ship in A battle.  Id is stable across battles, so combatId is also used to determine "uniqueness"
 */
public class Ship implements Comparable<Ship> {
    private static final Logger log = Global.getLogger(data.scripts.combatanalytics.data.Ship.class);

    public static final String NO_CAPTAIN = "No Captain";
    public static final String NO_NAME = "No Name";

    public String id;   // Should be the same as FleetMember.Id
    public String name;
    public ShipAPI.HullSize hullSize;
    public String hullClass;
    public int owner;

    public float maxHp;
    public float remainingHp; // we might want to track damage sustained, but that's a bit more work

    public int deploymentPoints; // unmodified value
    public String captain = NO_CAPTAIN;

    public String hullId;
    public ShipStatus status = ShipStatus.OK; // start as OK

    // only used by the intel report on initial construction
    public String captainSprite = "";

    public List<WeaponSet> weaponSets = new ArrayList<>();

    private Ship(){

    }

    //Constructed during combat
    public Ship(ShipAPI ship){
        id = ship.isFighter() || ship.isDrone() || ship.getFleetMemberId() == null ? Helpers.getSmallUuid() : ship.getFleetMemberId();
        name = coalesce(ship.getName(), id, NO_NAME);
        hullSize = ship.getHullSpec().getHullSize();
        hullClass = cleanUpDesignation(ship.getHullSpec().getHullName());

        owner = ship.getOriginalOwner();
        maxHp = ship.getMaxHitpoints();
        hullId = ship.getHullSpec().getHullId();

        if(hullSize == ShipAPI.HullSize.FIGHTER){
            name = hullClass +" Fighter";
        }
    }

    public Ship(FleetMemberAPI fm, String combatId){
        id = fm.isFighterWing() ? Helpers.getSmallUuid() : fm.getId();
        name = coalesce(fm.getShipName(), id, NO_NAME);
        hullSize = fm.getHullSpec().getHullSize();
        hullClass = cleanUpDesignation(fm.getHullSpec().getHullName());

        owner = fm.getOwner();
        maxHp = fm.getHullSpec().getHitpoints();
        hullId = fm.getHullSpec().getHullId();

        if(hullSize == ShipAPI.HullSize.FIGHTER){
            name = hullClass +" Fighter";
        }
    }

    public void setCaptain(PersonAPI captain){
        if(captain != null){
            this.captain = captain.getName().getFullName();
            this.captainSprite = captain.getPortraitSprite();
        }
    }

    // set post-combat
    public void setFleetMemberData(PersonAPI captain, int deploymentPoints, ShipStatus status, float remainingHp){
        setCaptain(captain);

        this.deploymentPoints = deploymentPoints;

        this.status = status;
        this.remainingHp = remainingHp;

        if(status.wasKilled()){
            this.remainingHp = 0;
        }
    }

    public Ship(String id, String name, ShipAPI.HullSize hullSize, String hullClass, float maxHitpoints,
                float maxFlux, int owner, int deploymentPoints, String captain, String hullId, ShipStatus shipStatus, String captainSprite, float remainingHp){
        this.id = id;
        this.name = name;
        this.hullSize = hullSize;
        this.hullClass = hullClass;
        this.owner = owner;
        this.deploymentPoints = deploymentPoints;
        this.maxHp = maxHitpoints;
        this.captain = captain;

        if(this.captain.equals("")){
            this.captain = NO_CAPTAIN;
        }
        this.captainSprite = captainSprite;
        this.hullId = hullId;
        this.status = shipStatus;
        this.remainingHp = remainingHp;
    }

    private String cleanUpDesignation(String designation) {
        // probably a better way to do do this, but I couldn't figure it out based on the docs
        int dashIndex = designation.lastIndexOf("-class");
        if(dashIndex > -1){
            return designation.substring(0, dashIndex);
        }

        dashIndex = designation.lastIndexOf(" class");
        if (dashIndex > -1) {
            return designation.substring(0, dashIndex);
        }
        return designation.trim();
    }

    public boolean hasCaptain(){
        return !this.captain.equals(NO_CAPTAIN);
    }

    public String toString(){
        return String.format("%1$s - %2$s - %3$s - %4$s", name, hullSize, hullClass, ownerAsString(owner));

        //"ISS ship - Destroyer - Medeusa - Player"
    }

    public String getHullSizeString(){
        switch (this.hullSize){
            case CAPITAL_SHIP: return "Capital";
            case CRUISER: return "Cruiser";
            case DESTROYER: return "Destroyer";
            case FRIGATE: return "Frigate";
            case FIGHTER: return "Fighter";
            default: return hullSize.toString();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ship ship = (Ship) o;

        return id.equals(ship.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public int compareTo(Ship o) {
        int ret = Integer.compare(this.owner, o.owner);
        if(ret == 0){
            ret = Integer.compare(this.deploymentPoints, o.deploymentPoints);
        }
        if(ret == 0){
            ret = this.name.compareTo(o.name);
        }

        return ret;
    }

    public double getRemainingHullPct(){
        if(remainingHp < .001){
            return 0d;
        }

        double ret = this.remainingHp / this.maxHp;
        if(Double.isNaN(ret) || Double.isInfinite(ret)){
            return 1;
        } else {
            return ret;
        }
    }

    private Object readResolve() {
        this.captain = captain.intern();
        this.name = name.intern();
        this.hullId = hullId.intern();
        this.hullClass = hullClass.intern();

        return this;
    }
}