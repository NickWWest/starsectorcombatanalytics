package data.scripts.combatanalytics.data;

public class WeaponSet {
    public String weapon;
    public int count;
    public int ordnancePointsPer;

    // we could change how we compute stats for built in weapons regarding relative weapon performance as some
    // built ins have comically bad values.  But really this just papers over shitty design choices of mods.
    public boolean isBuiltIn;

    public WeaponSet(){

    }

    public WeaponSet(String weaponName, int weaponCount, int ordnancePointsPerWeapon, boolean isBuiltIn) {

        // defend against stupid values from snarky mods
        if(ordnancePointsPerWeapon < 0){
            ordnancePointsPerWeapon = 1;
        }

        if(isBuiltIn && ordnancePointsPerWeapon > 100){
            ordnancePointsPerWeapon = 100;
        }

        this.weapon = weaponName;
        this.count = weaponCount;
        this.ordnancePointsPer = ordnancePointsPerWeapon;
        this.isBuiltIn = isBuiltIn;
    }

    @Override
    public String toString() {
        return "WeaponSet{" +
                "weapon='" + weapon + '\'' +
                ", count=" + count +
                ", ordnancePointsPer=" + ordnancePointsPer +
                '}';
    }

    private Object readResolve() {
        this.weapon = weapon.intern();

        return this;
    }
}
