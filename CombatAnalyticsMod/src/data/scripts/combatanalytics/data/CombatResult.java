package data.scripts.combatanalytics.data;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.ShipAPI;
import data.scripts.combatanalytics.function.AggregateProcessor;
import data.scripts.combatanalytics.function.DamageSet;
import data.scripts.combatanalytics.function.GroupedByShipDamage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static data.scripts.combatanalytics.util.Helpers.INT_FORMAT;
import static data.scripts.combatanalytics.util.Helpers.ISO_DATE_TIME;

public class CombatResult implements Comparable<CombatResult>{
    private static final Logger log = Global.getLogger(data.scripts.combatanalytics.data.CombatResult.class);

    public String combatId;
    public String faction;
    public String fleetName;
    public long engagementEndTime; //UTC

    public float combatDurationSeconds;

    public WeaponShipDamage[] weaponShipDamages;

    public WeaponCountingDamage[] weaponCountingDamages;

    public CombatGoal enemyFleetGoal;

    public Ship[] allShips;

    // empty consturctor for deserialization
    private CombatResult(){

    }

    public CombatResult(String combatId, String faction, String fleetName, long engagementEndTime,
                        Damage[] damages, float combatDurationSeconds, CombatGoal enemyFleetGoal, List<Ship> allShips) {
        this.combatId = combatId;
        this.faction = faction;
        this.fleetName = fleetName;
        this.engagementEndTime = engagementEndTime;
        this.combatDurationSeconds = combatDurationSeconds;

        this.enemyFleetGoal = enemyFleetGoal;

        this.allShips = allShips.toArray(new Ship[0]);

        WeaponShipDamage[] weaponTargetDamages = buildWeaponTargetDamages(damages, combatId);

        setWeaponDamages(weaponTargetDamages);

        log.debug("Computing combat results from " + damages.length + " Damages  =>  " + weaponShipDamages.length + " WeaponShipDamages  " + weaponCountingDamages.length + " WeaponCountingDamages  " + this.allShips.length + " Total Ships");
    }

    public int getEnemyFleetSize(){
        int ret = 0;
        for(Ship s : allShips){
            if(s.owner == 1 && s.hullSize != ShipAPI.HullSize.FIGHTER && s.hullSize != ShipAPI.HullSize.DEFAULT){
                ret++;
            }
        }

        return ret;
    }

    public int getEnemyDeploymentPoints(){
        int ret = 0;
        for(Ship s : allShips){
            if(s.owner == 1 && s.hullSize != ShipAPI.HullSize.FIGHTER && s.hullSize != ShipAPI.HullSize.DEFAULT){
                ret += s.deploymentPoints;
            }
        }

        return ret;
    }

    public String toString(){
        float totalDamage = 0;
        for(WeaponShipDamage wtd : weaponShipDamages){
            totalDamage += (wtd.armorDamage + wtd.hullDamage + wtd.shieldDamage);
        }

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(engagementEndTime);

        return String.format("%1$s %2$s (%3$s total ships involved) on %4$s  %5$s total damage exchanged",
                faction, fleetName, this.allShips.length, ISO_DATE_TIME.format(cal.getTime()), INT_FORMAT.format(totalDamage));
    }

    // aggregate the currentBattleDamage data removing the time component
    private static WeaponShipDamage[] buildWeaponTargetDamages(Damage[] damages, String combatId){
        // map of firing ship to groupName damage
        Map<String, WeaponShipDamage> keyToWtd = new HashMap<>();

        for (Damage dmg : damages) {

            // don't care about friendly fire
            if(dmg.firingShip.owner == dmg.targetShip.owner && !dmg.isHealing()){
                continue;
            }

            // we don't track killing flares, that's their job
            if(dmg.targetShip.hullSize == ShipAPI.HullSize.DEFAULT && dmg.targetShip.name.startsWith("Flare Launcher")){
                continue;
            }

            String key = dmg.firingShip.id + dmg.targetShip.id + dmg.weaponName;
            WeaponShipDamage wtd = keyToWtd.get(key);
            if(wtd == null){
                wtd = new WeaponShipDamage(dmg.firingShip, dmg.targetShip, dmg.weaponName);
                keyToWtd.put(key, wtd);
                log.debug("Creating new WeaponShipDamage entry for: " + dmg.targetShip +"-"+dmg.weaponName);
            }

            wtd.merge(dmg);
        }

        WeaponShipDamage[] ret = keyToWtd.values().toArray(new WeaponShipDamage[]{});

        Arrays.sort(ret);

        computePctDamageToTargets(ret);

        return ret;
    }

    private void setWeaponDamages(WeaponShipDamage[] wsds) {
        List<WeaponCountingDamage> countingDamages = new ArrayList<>();
        List<WeaponShipDamage> shipDamages = new ArrayList<>();

        Map<Ship, GroupedByShipDamage> shipToStats = new HashMap<>();

        // aggregateWeaponTargetDamages ignores friendly fire
        AggregateProcessor.aggregateWeaponTargetDamages(wsds, new WeaponCountingDamage[0], shipToStats);

        for(Map.Entry<Ship, GroupedByShipDamage> shipToDamage : shipToStats.entrySet()){
            if(shipToDamage.getKey().hullSize.ordinal() < ShipAPI.HullSize.FRIGATE.ordinal()){
                continue;
            }

            for(Map.Entry<String, DamageSet> weaponToDamage : shipToDamage.getValue().weaponNameToDamage.entrySet()) {
                int fighterKills = weaponToDamage.getValue().aggregateFighters().getMajorityKills().size();
                int missileKills = weaponToDamage.getValue().aggregateMissiles().getMajorityKills().size();
                if(fighterKills > 0 || missileKills > 0) { // only track if there's something to track
                    countingDamages.add(new WeaponCountingDamage(shipToDamage.getKey(), weaponToDamage.getKey(), missileKills, fighterKills));
                }
            }
        }

        for(WeaponShipDamage wsd : wsds){
            if(wsd.target.hullSize != ShipAPI.HullSize.FIGHTER && wsd.target.hullSize != ShipAPI.HullSize.DEFAULT){
                wsd.prepForSave();
                shipDamages.add(wsd);
            }
        }

        this.weaponShipDamages = shipDamages.toArray(new WeaponShipDamage[0]);
        this.weaponCountingDamages = countingDamages.toArray(new WeaponCountingDamage[0]);
    }

    public static void computePctDamageToTargets(WeaponShipDamage[] wsds){
        Map<Ship, Double> shipToTotalDamage = new HashMap<>();
        // build up our totals
        for(WeaponShipDamage wtd : wsds){
            if(!wtd.target.status.wasKilled()){
                continue;
            }

            if((wtd.hullDamage + wtd.armorDamage) < 0){
                continue;
            }

            Double d = shipToTotalDamage.get(wtd.target);
            if(d == null){
                d = 0d;
            }

            d += wtd.hullDamage + wtd.armorDamage;
            shipToTotalDamage.put(wtd.target, d);
        }

        // assign pcts
        for(WeaponShipDamage wsd : wsds){
            Double totalDmgToShip = shipToTotalDamage.get(wsd.target);
            if(totalDmgToShip == null){
                continue;
            }

            if(totalDmgToShip >= 1) {
                wsd.pctOfDamageDoneToTarget = (wsd.hullDamage + wsd.armorDamage) / totalDmgToShip;
            }
        }
    }

    @Override
    public int compareTo(CombatResult combatResult) {
        // descending is natural sort
        return Long.compare(combatResult.engagementEndTime, this.engagementEndTime);
    }

    private Object readResolve() {
        faction = faction.intern();
        fleetName = fleetName.intern();

        return this;
    }
}
