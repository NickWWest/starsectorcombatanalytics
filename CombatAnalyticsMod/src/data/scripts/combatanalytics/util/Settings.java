package data.scripts.combatanalytics.util;

import com.fs.starfarer.api.Global;
import lunalib.lunaSettings.LunaSettings;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.lwjgl.input.Keyboard;

/**
 * Represents data found in CombatAnalytics settings.json
 */
public class Settings {

    private static final Logger log = Global.getLogger(data.scripts.combatanalytics.util.Settings.class);
    
    private static final boolean LunaEnabled = Global.getSettings().getModManager().isModEnabled("lunalib");
    
    public static final String DetailedCombatResultsLoggingLevel = getStringSetting("dcr_LoggingLevel", Level.INFO.toString());

    public static boolean shouldUninstall(){
        return getBoolean("dcr_UninstallMode", false);
    }

    public static int getCombatAnalyzeKeyCode(){
        return getKeyCode("dcr_AnalyzeKey", Keyboard.KEY_L);
    }
    
    public static boolean getFighterWeaponBreakout(){
        return getBoolean("dcr_FighterWeaponBreakout", false);
    }
    
    public static int getMaxCombatResultCount(){
        return getInteger("dcr_MaxCombatResultCount", 50);
    }

    public static int getCombatResultLifetimeInDays(){
        return getInteger("dcr_CombatResultLifetimeInDays", 750);
    }

    public static int getAggregateResultLifetimeInDays(){
        return getInteger("dcr_AggregateResultLifetimeInDays", 5);
    }

    public static int getSimulationResultLifetimeInDays(){
        return getInteger("dcr_SimulationResultLifetimeInDays", 1);
    }

    public static int getShipCountLimit(){
        return getInteger("dcr_ShipCountLimit", 15);
    }

    private static String getStringSetting(String setting, String defaultValue){
        if (LunaEnabled)
        {
            String ret = LunaSettings.getString("DetailedCombatResults", setting);
            if(ret == null){
                return defaultValue;
            }
            return ret;
        }

        return defaultValue;
    }

    public static int getKeyCode(String setting, int defaultValue)
    {
        int ret = getInteger(setting, defaultValue);
        if(ret == 0){ // Luna returns 0 if the keycode is bad
            return defaultValue;
        }
        
        return ret;
    }

    public static int getInteger(String setting, int defaultValue)
    {
        if (LunaEnabled)
        {
            Integer ret = LunaSettings.getInt("DetailedCombatResults", setting);
            if(ret == null){
                return defaultValue;
            }
            return ret;
        }

        return defaultValue;
    }

    private static boolean getBoolean(String setting, boolean defaultValue)
    {
        if (LunaEnabled)
        {
            Boolean ret = LunaSettings.getBoolean("DetailedCombatResults", setting);
            if(ret == null){
                return defaultValue;
            }
            return ret;
        }

        return defaultValue;
    }

    public static boolean UseReportedDamagesOnly(){
        try {
            return Global.getSettings().getBoolean("DetailedCombatResults_UseReportedDamagesOnlyV1");
        } catch(Throwable t){
            return false;
        }
    }
}
