package data.scripts.combatanalytics;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.util.IntervalUtil;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.reflection.PureJavaReflectionProvider;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import data.scripts.combatanalytics.data.CombatGoal;
import data.scripts.combatanalytics.data.CombatResult;
import data.scripts.combatanalytics.data.Ship;
import data.scripts.combatanalytics.data.WeaponCountingDamage;
import data.scripts.combatanalytics.data.WeaponSet;
import data.scripts.combatanalytics.data.WeaponShipDamage;
import data.scripts.combatanalytics.util.Base64;
import data.scripts.combatanalytics.util.CompressionUtil;
import data.scripts.combatanalytics.util.Helpers;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static data.scripts.combatanalytics.util.Helpers.TWO_DIGIT_NO_GROUP_FORMAT;

/*
 * Manages the serialization and deserialization of data in a string format to prevent breakage when the mod is removed
 * or when the data format changes (allows for migration)
 */
public class SerializationManager {

    private static final String COMBATRESULTS_KEY = "CombatAnalytics_CombatResults_V4";

    private static final String COMBATRESULTS_KEY_OLD = "CombatAnalytics_CombatResults_V3";
    private static final String SHIP_KEY_OLD = "CombatAnalytics_Ships_V3";
    private static final String WEAPONDAMAGE_KEY_OLD = "CombatAnalytics_WeaponDamage_V3";

    private final static Logger log = Global.getLogger(data.scripts.combatanalytics.SerializationManager.class);
    static Map<String, Object> overridePersistentData = null;  // only set by unit tests

    // data lacks integrity, clear and re-write data
    public static boolean dataIsCorrupt = false;

    private static List<CombatResult> _resultCache = null;

    public static void saveCombatResult(CombatResult cr){
        try {
            List<CombatResult> combatResults = getAllSavedCombatResults();
            combatResults.add(cr);
            Collections.sort(combatResults);

            String rawCombatResults = getSerializer().toXML(combatResults);

            saveValue(COMBATRESULTS_KEY, rawCombatResults);
        }
        catch (Throwable e){
            log.error("Unable to save combat result", e);
            Helpers.printErrorMessage("Unable to save combat result");
        }
    }

    public static void onGameLoad(){
        // clear any data saved in statics
        clearCache();
        dataIsCorrupt = false;

        // upgrade old data if any & possible
        Map<String, Object> pd = getPersistentData();

        // remove old
        pd.remove(SHIP_KEY_OLD);
        pd.remove(WEAPONDAMAGE_KEY_OLD);
        pd.remove(COMBATRESULTS_KEY_OLD);
    }

    public static void clearSavedData(){
        saveValue(COMBATRESULTS_KEY, "");

        dataIsCorrupt = false;
        clearCache();
    }

    private static void saveValue(String key, String value){
        value = compress(value);
        getPersistentData().put(key, value);
    }

    static String compress(String s){
        int originalSize = s.length();
        byte[] compressedString = CompressionUtil.compress(s);
        return Base64.convert(compressedString);
    }

    static String decompress(String s){
        if(s == null){
            return null;
        }

        byte[] bytes = Base64.convert(s);
        return CompressionUtil.decompress(bytes);
    }

    public static long sizeOfSavedDataInBytes(){
        long ret = 0;
        String rawValues = (String) getPersistentData().get(COMBATRESULTS_KEY);
        if(rawValues != null){
            ret += rawValues.length();
        }

        return ret * 2; // ret is char count, 2 byte chars in Java
    }

    public static List<CombatResult> getSavedCombatResults(Set<String> combatResultIds){
        return getSavedCombatResults(new CombatResultFilter(combatResultIds));
    }

    public static List<CombatResult> getAllSavedCombatResultsNoSimulation(){
        List<CombatResult> allCombats = getAllSavedCombatResults();
        List<CombatResult> ret = new ArrayList<>(allCombats.size());

        for(CombatResult cr : allCombats){
            if(cr.enemyFleetGoal != CombatGoal.SIMULATION){
                ret.add(cr);
            }
        }

        return ret;
    }

    public static void clearCache(){
        _resultCache = null;
    }

    private static XStream getSerializer(){
        XStream x = new XStream(new PureJavaReflectionProvider(), new StaxDriver(), SerializationManager.class.getClassLoader());
        x.alias("CombatResult", CombatResult.class);

        x.alias("Ship", Ship.class);
        x.aliasAttribute(Ship.class, "deploymentPoints", "dp");
        x.aliasAttribute(Ship.class, "captainSprite", "cs");
        x.aliasAttribute(Ship.class, "weaponSets", "ws");
        x.aliasAttribute(Ship.class, "remainingHp", "rhp");
        x.aliasAttribute(Ship.class, "maxHp", "mhp");

        x.alias("WeaponSet", WeaponSet.class);
        x.aliasAttribute(WeaponSet.class, "weapon", "w");
        x.aliasAttribute(WeaponSet.class, "count", "c");
        x.aliasAttribute(WeaponSet.class, "ordnancePointsPer", "o");

        x.alias("WSD", WeaponShipDamage.class);
        x.aliasAttribute(WeaponShipDamage.class, "source", "s");
        x.aliasAttribute(WeaponShipDamage.class, "target", "t");
        x.aliasAttribute(WeaponShipDamage.class, "weapon", "w");
        x.aliasAttribute(WeaponShipDamage.class, "hits", "h");
        x.aliasAttribute(WeaponShipDamage.class, "shieldDamage", "sd");
        x.aliasAttribute(WeaponShipDamage.class, "armorDamage", "ad");
        x.aliasAttribute(WeaponShipDamage.class, "hullDamage", "hd");
        x.aliasAttribute(WeaponShipDamage.class, "empDamage", "ed");
        x.aliasAttribute(WeaponShipDamage.class, "pctOfDamageDoneToTarget", "p");
        x.aliasAttribute(WeaponShipDamage.class, "killingBlow", "k");


        x.alias("WCD", WeaponCountingDamage.class);
        x.aliasAttribute(WeaponCountingDamage.class, "source", "s");
        x.aliasAttribute(WeaponCountingDamage.class, "weapon", "w");
        x.aliasAttribute(WeaponCountingDamage.class, "missileKills", "mk");
        x.aliasAttribute(WeaponCountingDamage.class, "fighterKills", "fk");

        return x;
    }

    public static List<CombatResult> getAllSavedCombatResults(){
        if(_resultCache == null){
            long loadTimeInMs = System.currentTimeMillis();

            try {
                XStream xStream = getSerializer();
                String rawValues = (String) getPersistentData().get(COMBATRESULTS_KEY);

                String rawCombatResults = decompress(rawValues);

                List<CombatResult> combatResults;
                if(rawCombatResults == null || rawCombatResults.length() == 0){
                    combatResults = new ArrayList<>();
                    rawCombatResults = "";
                    rawValues = "";
                } else {
                    combatResults = (List<CombatResult>) xStream.fromXML(rawCombatResults);
                }

                Collections.sort(combatResults);

                _resultCache = combatResults;

                loadTimeInMs = System.currentTimeMillis() - loadTimeInMs;

                long compressedSize = rawValues.length();
                long decompressedSize = rawCombatResults.length();
                double compression = compressedSize / (double) decompressedSize;
                int compressionPercent = (int)(compression * 100);

                log.info("Loaded " + _resultCache.size() + " prior battle results using " +
                        TWO_DIGIT_NO_GROUP_FORMAT.format(SerializationManager.sizeOfSavedDataInBytes() / (1024 * 1024d)) + "MB in save file" +
                        " ("+compressionPercent+"% of original size from compression)" +
                        " in "+loadTimeInMs+"ms");
            }
            catch (Throwable e){
                dataIsCorrupt = true;
                log.error("Unable to load SavedCombatResults", e);
                Helpers.printErrorMessage("Error loading saved combat results");
                return new ArrayList<>();
            }
        }

        return _resultCache;
    }

    private static List<CombatResult> getSavedCombatResults(CombatResultFilter filter){
        List<CombatResult>  all = getAllSavedCombatResults();
        List<CombatResult> ret = new ArrayList<>(all.size());

        for(CombatResult cr : all){
            if(filter.isCombatResultValid(cr.combatId)){
                ret.add(cr);
            }
        }

        return ret;
    }

    private static Map<String, Object> getPersistentData(){
        if(overridePersistentData != null){
            return overridePersistentData;
        }
        return Global.getSector().getPersistentData();
    }

    private static class CombatResultFilter{
        private final Set<String> _combatResultIds;

        CombatResultFilter(Set<String> combatResultIds){
            _combatResultIds = combatResultIds;
        }

        CombatResultFilter(){
            _combatResultIds = null;
        }

        public boolean isCombatResultValid(String combatResult){
            return _combatResultIds == null || _combatResultIds.contains(combatResult);
        }
    }

    private static class StringInterner {
        private final HashMap<String, String> _internMap = new HashMap<>(1000);

        public String intern(String s){
            String ret = _internMap.get(s);
            if(ret == null){
                _internMap.put(s, s);
                ret = s;
            }

            return ret;
        }

        public void internElements(String[] arr){
            for(int i=0; i<arr.length; i++){
                arr[i] = intern(arr[i]);
            }
        }

        @Override
        public String toString() {
            return "StringInterner{" +
                    "InternMapSize=" + _internMap.size() +
                    '}';
        }
    }
}
