package data.scripts.combatanalytics;

import com.fs.starfarer.api.ui.TooltipMakerAPI;
import data.scripts.combatanalytics.data.Ship;
import data.scripts.combatanalytics.data.WeaponSet;
import data.scripts.combatanalytics.function.AggregateDamage;
import data.scripts.combatanalytics.util.Localization;

import java.awt.Color;

public class WeaponTooltipCreator implements TooltipMakerAPI.TooltipCreator {

    private final Ship ship;
    private final AggregateDamage _aggregateDamage;

    public WeaponTooltipCreator(Ship ship, AggregateDamage ad) {
        this.ship = ship;
        this._aggregateDamage = ad;
    }

    @Override
    public boolean isTooltipExpandable(Object tooltipParam) {
        return false;
    }

    @Override
    public float getTooltipWidth(Object tooltipParam) {
        return 450;
    }

    @Override
    public void createTooltip(TooltipMakerAPI tooltip, boolean expanded, Object tooltipParam) {
        Color valueColor = Color.WHITE;
        //todo: I18n
        WeaponSet ws = getWeaponSet(_aggregateDamage.groupName);

        if(ws != null){
            tooltip.addPara(Localization.WeaponCount, 0f, valueColor, ""+ws.count);

            int totalOpForWeapon = ws.count * ws.ordnancePointsPer;
            tooltip.addPara(Localization.WeaponOpTotal, 0f, valueColor, ""+totalOpForWeapon );

            int totalWeaponOpForShip = getTotalWeaponOpForShip();

            if(totalOpForWeapon > 0 && totalWeaponOpForShip > 0) {
                int pctOfOp = (int) (totalOpForWeapon * 100 / (double) totalWeaponOpForShip);
                tooltip.addPara(Localization.WeaponOpAsPct, 0f, valueColor, "" + pctOfOp);
            }
        }

        tooltip.addPara(Localization.KillingBlows, 0f, valueColor, _aggregateDamage.getKillingBlows() + "");

        //todo Gather proper data and do this
//        tooltip.addPara("Shots Fired: %s", 0f, Color.WHITE, 1+"");
//        tooltip.addPara("Accuracy: %s", 0f, Color.WHITE, 1+"");
    }

    public int getTotalWeaponOpForShip(){
        int totalWeaponOpForShip = 0;
        for(WeaponSet w : ship.weaponSets){
            totalWeaponOpForShip += (w.count * w.ordnancePointsPer);
        }

        return totalWeaponOpForShip;
    }

    public float getWeaponGroupOpAsPct(){
        WeaponSet ws = getWeaponSet(_aggregateDamage.groupName);
        if(ws == null){
            return 0;
        }

        int totalOpForWeapon = ws.count * ws.ordnancePointsPer;
        int totalOpForShip = getTotalWeaponOpForShip();

        if(totalOpForWeapon < 1 || totalOpForShip < 1){
            return 0;
        }

        return (float) (totalOpForWeapon * 100 / (double) totalOpForShip);
    }

    private WeaponSet getWeaponSet(String weaponName){
        for(WeaponSet ws : ship.weaponSets){
            if(ws.weapon.equals(weaponName)){
                return ws;
            }
        }

        return null;
    }
}
