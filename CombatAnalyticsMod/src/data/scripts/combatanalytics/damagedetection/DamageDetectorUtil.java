package data.scripts.combatanalytics.damagedetection;

import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;

public class DamageDetectorUtil {

    public static ShipAPI getResponsibleShip(ShipAPI ship){
        // see if it's a DEM missile beam.  Those originate from drones that are spawned from the missile to fire the beam
        if(ship.isDrone() && ship.getDroneSource() == null && ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.DRONE_MOTHERSHIP)){
            ShipAPI ret = (ShipAPI) ship.getAIFlags().getCustom(ShipwideAIFlags.AIFlags.DRONE_MOTHERSHIP);
            if (ret != null) {
                return ret;
            }
        }

        return ship;
    }
}
