package data.scripts.combatanalytics.damagedetection;

import data.scripts.combatanalytics.damagedetection.ListenerDamageInference.SourceTargetWeapon;
import data.scripts.combatanalytics.util.LRUMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Instead of storing each damage event, aggregate them as they happen based on SourceTargetWeapon
 */
public class ReportableDamageSet {
    private final HashMap<SourceTargetWeapon, ReportableDamage> _combatDamages = new HashMap<>(10000);
    private LRUMap<Integer, Integer> _processedHashcodes = null;
    private final boolean _enforceUniqueness;

    public ReportableDamageSet(boolean enforceUniqueness){
        this._enforceUniqueness = enforceUniqueness;

        if(enforceUniqueness){
            _processedHashcodes = new LRUMap<>(10000, 25000);
        }
    }

    public List<ReportableDamage> getAggregatedCombatDamages(){
        return new ArrayList<>(_combatDamages.values());
    }

    public void addAllReportableDamages(List<ReportableDamage> rds){
        for (ReportableDamage rd : rds){
            addReportableDamage(rd);
        }
    }

    public void addReportableDamage(ReportableDamage rd){

        // used by projectiles only, if we've seen this before, don't use it
        if(_enforceUniqueness){
            if(_processedHashcodes.containsKey(rd.damagingEntityId)){
                return;
            } else {
                _processedHashcodes.put(rd.damagingEntityId, rd.damagingEntityId);
            }
        }

        SourceTargetWeapon stw = new SourceTargetWeapon(rd.sourceShip, rd.targetShip, rd.weaponName);

        ReportableDamage savedDamage = _combatDamages.get(stw);
        if(savedDamage == null){
            _combatDamages.put(stw, rd);
        } else {
            savedDamage.listenerDamage.addDamage(rd.listenerDamage);
            savedDamage.wasKillingBlow |= rd.wasKillingBlow;
        }
    }

    public int size(){
        return _combatDamages.size();
    }

    @Override
    public String toString() {
        return "ReportableDamageSet{" +
                "_combatDamages=" + _combatDamages.size() +
                '}';
    }
}
