package data.scripts.combatanalytics.damagedetection;

import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import org.lwjgl.util.vector.Vector2f;

class FrameDamageClarification extends FrameDamage {

    private final RawDamage _damage;
    private final String _weaponName;
    private final ShipAPI _source;
    private final ShipAPI _target;

    public FrameDamageClarification(ShipAPI source, ShipAPI target, float damage, float empDamage, DamageType damageType, String weaponName){
        _source = source;
        _target = target;
        _damage = new RawDamage(damage, empDamage, damageType);
        _weaponName = weaponName;
    }

    @Override
    public String getWeaponName() {
        return _weaponName;
    }

    @Override
    public RawDamage getRawDamage() {
        return _damage;
    }

    @Override
    public ShipAPI getTarget() {
        return _target;
    }

    @Override
    public ShipAPI getSource() {
        return _source;
    }

    @Override
    public DamageType getDamageType() {
        return _damage.type;
    }

    @Override
    public Vector2f getLocation() {
        return new Vector2f();
    }

    @Override
    public int hashCode() {
        return _source.hashCode() + _target.hashCode() + _weaponName.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FrameDamageClarification that = (FrameDamageClarification) o;

        return _source.equals(that._source) && _target.equals(that._target) && _weaponName.equals(that._weaponName) && _damage.compareTo(that._damage) == 0;
    }

    @Override
    public DamagingProjectileAPI getProjectile(){
        return null;
    }
}
