package data.scripts;

import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.MissileAPI;

/**
 * Contains the {@code float} armor damage and {@code float} hull damage a
 * {@link DamagingProjectileAPI}, {@link MissileAPI}, or {@link BeamAPI} has inflicted.
 * <p></p>
 * Create a DamageReport after calculating the armor and hull damage and pass it to
 * DamageReportManagerV1.getDamageReportManager().addDamageReport() for DetailedCombatResults to record and consume.
 */
public class DamageClarificationV1 {

    public static Object[] serialize(DamageClarificationV1 report){
        return new Object[]{
                report.shipDamage,
                report.empDamage,
                report.damageType,
                report.source,
                report.target,
                report.weaponName,
        };
    }

    public static DamageClarificationV1 deserialize(Object[] raw){
        return new DamageClarificationV1(
                (float)raw[0],
                (float)raw[1],
                (DamageType)raw[2],
                (CombatEntityAPI)raw[3],
                (CombatEntityAPI)raw[4],
                (String)raw[5]
        );
    }

    // we know it hit the ship, no idea if it was hull, armor or shield
    private final float shipDamage;
    private final float empDamage;


    private final DamageType damageType;
    private final CombatEntityAPI source;
    private final CombatEntityAPI target;
    private final String weaponName;

    public DamageClarificationV1(float shipDamage, float empDamage, DamageType damageType, CombatEntityAPI source, CombatEntityAPI target, String weaponName) {
        this.shipDamage = shipDamage;
        this.empDamage = empDamage;
        this.damageType = damageType;
        this.source = source;
        this.target = target;
        this.weaponName = weaponName;
    }

    /**
     * Returns the {@code float} damage a {@link DamagingProjectileAPI},
     * {@link MissileAPI}, or {@link BeamAPI} has inflicted.
     */
    public float getShipDamage() { return shipDamage; }

    /**
     * Returns the {@code float} emp damage a {@link DamagingProjectileAPI},
     * {@link MissileAPI}, or {@link BeamAPI} has inflicted.
     */
    public float getEmpDamage() {return empDamage;}

    public CombatEntityAPI getSource() { return source; }


    public DamageType getDamageType() {
        return damageType;
    }

    public CombatEntityAPI getTarget() {
        return target;
    }

    public String getWeaponName() {
        return weaponName;
    }

    public boolean isValid(){
        return source != null && target != null && weaponName != null;
    }
}
