**Detailed Combat Results**

https://bitbucket.org/NickWWest/starsectorcombatanalytics

Enables ship and weapon analysis of combat effectiveness

**FOR MODDERS**

If you have weapons that are doing unusual things where their damage (or repairs) aren't being recorded by DCR
consider creating a copy of the class: [/CombatAnalyticsMod/src/data/scripts/DCRIntegration.java](/CombatAnalyticsMod/src/data/scripts/DCRIntegration.java)
in your project and using it per the instructions in your own code.  This does not require a dependency on DCR to function.

For an example of this in action, see how it's used in ED Shipyards:
https://bitbucket.org/NickWWest/edshipyard/src/develop/src/data/dcr/edshipyard/DamageReportManagerV1.java
   
**How this mod works**

* There exists an EveryFrameCombatPlugin that watches for damage being dealt (DamageDetectionEveryFrameCombatPlugin.java and EveryFrameDamageDetector.java)
* This is a bit trickier than it sounds due to beams, missiles and projectiles all behaving in slightly different ways
* For every damage that is dealt, create an object to track that individual damage (Damage.java), this is our "fact" table

**License**: MIT - Do what ever you want, I'm not liable